package com.itau.lancamento.produtos.controllers;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.lancamento.produtos.services.Sender;

@RestController
public class MensagemProdutoController {

	@RequestMapping(method = RequestMethod.POST, path = "/envio")
	public ResponseEntity<?> envialancamento(@Valid @RequestBody MensagemProdutoController mensagemproduto) {
		//Sender.sendMessage(mensagemproduto);
		try {
			return ResponseEntity.ok().body(mensagemproduto);

		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
			return ResponseEntity.badRequest().build();

		}
	}

}
