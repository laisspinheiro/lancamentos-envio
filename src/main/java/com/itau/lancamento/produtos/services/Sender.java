package com.itau.lancamento.produtos.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.lancamento.produtos.controllers.MensagemProdutoController;
import com.itau.lancamento.produtos.models.MensagemProduto;


@Component
public class Sender {
	
	@Autowired
	private ConnectionFactory connectionFactory;
	private static JmsTemplate jmsTemplate;
			
	@PostConstruct
	public void init() {
		Sender.jmsTemplate = new JmsTemplate(connectionFactory);
	}
	
	public static void sendMessage() {
		
		ObjectMapper eventos = new ObjectMapper();

		ArrayList<MensagemProduto> mocks = new ArrayList<>();
		
		MensagemProduto mensagemProduto1 = new MensagemProduto();
		mensagemProduto1.setAcao("DDA");
		mensagemProduto1.setContrato("Banco: 341 Agencia: 1001 Conta: 04427-4");
		mensagemProduto1.setCpf("35351062810");
		mensagemProduto1.setData(LocalDate.now());
		mensagemProduto1.setDescricao("Longo Prazo Renda Fixa");
		mensagemProduto1.setObservacao("Renda Fixa - 6,44% rent.");
		mensagemProduto1.setSigla("CC");
		mensagemProduto1.setValor(29800.00);
		mensagemProduto1.setTipoOperacao("Aplicar Investimento");
		mensagemProduto1.setMeta("Viajar em 2030");
		mensagemProduto1.setStatus("Operacao efetivada");
		
		MensagemProduto mensagemProduto2 = new MensagemProduto();
		mensagemProduto2.setAcao("Debito Automatico");
		mensagemProduto2.setContrato("Banco: 341 Agencia: 1001 Conta: 04427-4");
		mensagemProduto2.setCpf("35351062810");
		mensagemProduto2.setData(LocalDate.now());
		mensagemProduto2.setDescricao("Itau Renda Fixa IMA-B 5");
		mensagemProduto2.setObservacao("Itau Renda fixa - 6,65% rent.");
		mensagemProduto2.setSigla("CC");
		mensagemProduto2.setValor(29800.00);
		mensagemProduto2.setTipoOperacao("Aplicar investimento");
		mensagemProduto2.setMeta("Resgatar 50 mil em 3 anos");
		mensagemProduto2.setStatus("Lancamento agendado");
		
		MensagemProduto mensagemProduto3 = new MensagemProduto();
		mensagemProduto3.setAcao("DDA");
		mensagemProduto3.setContrato("Banco: 341 Agencia: 1001 Conta: 04427-4");
		mensagemProduto3.setCpf("35351062810");
		mensagemProduto3.setData(LocalDate.now());
		mensagemProduto3.setDescricao("Itau Renda Fixa IMA-B 5");
		mensagemProduto3.setObservacao("Itau Renda fixa - 6,65% rent.");
		mensagemProduto3.setSigla("CC");
		mensagemProduto3.setValor(29800.00);
		mensagemProduto3.setTipoOperacao("Aplicar investimento");
		mensagemProduto3.setMeta("Resgatar 50 mil em 3 anos");
		mensagemProduto3.setStatus("Operacao rejeitada - Saldo Insuficiente");
		
		mocks.add(mensagemProduto1);
		mocks.add(mensagemProduto2);
		mocks.add(mensagemProduto3);
		
		
		for (MensagemProduto mensagemProduto : mocks) {
			try {
				String msgJson;
				msgJson = eventos.writerWithDefaultPrettyPrinter().writeValueAsString(mensagemProduto);
				System.out.println("Chegou no metodo de fila");		
				jmsTemplate.convertAndSend("produtos.queue.lancamentofinanceiro",msgJson);
				System.out.println("Enviou para a fila");
				
			} catch (JsonProcessingException e) {
				System.out.println("Erro no envio da fila");
			}
		
		}
	}

}
