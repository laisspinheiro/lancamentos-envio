package com.itau.lancamento.produtos.controller;

import static org.junit.Assert.assertNotNull;

import java.sql.Date;
import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.metadata.TomcatDataSourcePoolMetadata;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.itau.lancamento.produtos.controllers.MensagemProdutoController;
import com.itau.lancamento.produtos.models.MensagemProduto;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MensagemProdutoControllerTest {
//	@Autowired
//	MensagemProduto mensagemProduto;
	
	@LocalServerPort
	private int port;
	
	@Autowired
	TestRestTemplate restTemplate;
	
	@Test
	public void enviarMensagem() throws Exception{
		String url = String.format("http://localhost:%s/envio", port);
		ResponseEntity<MensagemProduto> mensagem = restTemplate.postForEntity(url, mensagemTeste(), MensagemProduto.class);
		assertNotNull(mensagem.getBody());
		
	}


	public MensagemProduto mensagemTeste() {
		MensagemProduto mensagemProduto = new MensagemProduto();
		mensagemProduto.setAcao("D");
		mensagemProduto.setContrato("Banco: 341 Agencia: 1001 Conta: 04427-4");
		mensagemProduto.setCpf("35351062810");
		//mensagemProduto.setData(LocalDate.parse("2018, 08, 25",java.util.Date));
		mensagemProduto.setDescricao("Renegociacao");
		mensagemProduto.setObservacao("Parcela 1/4");
		mensagemProduto.setSigla("CC");
		mensagemProduto.setValor(29800.00);
		mensagemProduto.setTipoOperacao("Pagamento");
		return mensagemProduto;	
	}

}
