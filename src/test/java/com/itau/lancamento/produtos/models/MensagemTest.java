package com.itau.lancamento.produtos.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MensagemTest {

	@Test
	public void mensagemum() {
		
		MensagemProduto mensagemProduto = new MensagemProduto();
		mensagemProduto.setAcao("D");
		mensagemProduto.setContrato("Banco: 341 Agencia: 1001 Conta: 04427-4");
		mensagemProduto.setCpf("35351062810");
		//mensagemProduto.setData(LocalDate.parse("2018, 08, 25",java.util.Date));
		mensagemProduto.setDescricao("Renegociacao");
		mensagemProduto.setObservacao("Parcela 1/4");
		mensagemProduto.setSigla("CC");
		mensagemProduto.setValor(29800.00);
		mensagemProduto.setTipoOperacao("Pagamento");

		
		assertEquals("D", mensagemProduto.getAcao());
		
	}
}
